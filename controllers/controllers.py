import os

import pandas as pd
from odoo import http
from odoo.http import request, route
from werkzeug.exceptions import BadRequest
from ..models.prediction_results import PredictionResult


def fill_methods_from_df_and_csv(output_df):
    current_file_path = os.path.dirname(__file__)
    relative_path = '../static/src/method_descriptions.csv'
    file_path = os.path.join(current_file_path, relative_path)
    df = pd.read_csv(file_path, sep=';')
    methods = []
    for entry in output_df:
        # Parse the string representation of the list to actual list
        entry_list = eval(entry)
        method_name = entry_list[1]  # The method name is at index 1
        if df['Name'].isin([method_name]).any():
            method_info = df.loc[df['Name'] == method_name].iloc[
                0]  # Direktes Zugreifen auf die erste passende Zeile
            methods.append({
                'name': method_name,
                'description': method_info['Description'],
                'task_types': method_info['Task Types'],
                'advantages': method_info['Advantages'],
                'disadvantages': method_info['Disadvantages']
            })
    return methods


class OwlPlayground(http.Controller):

    @http.route(['/ai_assistant'], type='http', auth='public')
    def show_playground(self):
        """
        Renders the ai_assistant page
        """
        return request.render('ai_assistant.playground_content')

    @http.route(['/ai_assistant/home'], type='http', auth='public')
    def show_home(self):
        """
        Renders the page for home
        """
        return request.render('ai_assistant.home_content')

    @http.route('/ai_assistant/input', type='http', auth='public', methods=['GET'], website=True)
    def input_form(self):
        return request.render('ai_assistant.input_template')
    
    @http.route('/ai_assistant/history', type='http', auth='public', methods=['GET'], website=True)
    def history(self):
        history_entries = request.env['ai_assistant.model_user_input'].search([])
        formatted_entries = [{
            'id': entry.id,
            'create_date': entry.create_date.strftime('%Y-%m-%d'),
            'customer_name': entry.customer_name,
            'customer_id': entry.customer_id,
            'customer_loc': entry.customer_loc,
            'customer_contact': entry.customer_contact,
            'result_output': entry.result_output,
            # Include other fields as needed
        } for entry in history_entries]

        return request.render('ai_assistant.history_template', {
            'history_entries': formatted_entries
        })

    @http.route('/ai_assistant/process', type='http', auth='public', methods=['POST'], website=True)
    def process_data(self, **kwargs):
        # Save all the chosen options into the session
        chosen_options = {key: val for key, val in kwargs.items() if key.startswith('input_') or key in ['customer_id', 'customer_name', 'customer_loc', 'customer_contact','ki_methods_count', 'data_type', 'time_availability', 'computing_power', 'data_quality', 'data_format', 'dimensions_amount', 'accuracy_claim', 'type_goalsize', 'data_amount']}

        try:
            chosen_options['ki_methods_count'] = int(chosen_options.get('ki_methods_count', 0))
        except ValueError:
            # Handle the case where 'ki_methods_count' is not a valid integer
            raise BadRequest("Ungültiger Wert für ki_methods_count")

        request.session['result_data'] = chosen_options

        return request.redirect('/ai_assistant/result')

    @http.route('/ai_assistant/result', type='http', auth='public', website=True)
    def show_result(self):
        # Retrieve all chosen options from the session to display them on the result page
        result_data = request.session.get('result_data')
        if result_data:
            # Create a record in UserInput with the result_data
            user_input_record = request.env['ai_assistant.model_user_input'].create(result_data)

            result = PredictionResult(result_data)
            result.create_result_df()

            print(f"this is result_data: {result_data}")
            # Update the record with the prediction result
            print(f"this is result_output: {result.output_df}")

            methods = fill_methods_from_df_and_csv(result.output_df)
            user_input_record.write({'result_output': str(methods[0]['name'])})
            print(f"this is methods: {methods[0]['name']}")

            # In your show_result method
            return request.render('ai_assistant.result_template', {
                'methods': methods,
                'customer_id': result_data.get('customer_id', 'N/A'),  # Fetch 'customer_id' from result_data or default to 'N/A'
                'customer_name': result_data.get('customer_name', 'N/A')  # Fetch 'customer_name' from result_data or default to 'N/A'
            })