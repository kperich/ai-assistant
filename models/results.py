from odoo import models, fields

class Results(models.Model):
    _name = 'ai_assistant.model_results_model'
    _description = 'Ergebnisse der Abfrage'

    ergebnis_text = fields.Text(string="Ergebnis Text")
    zufriedenheit = fields.Char(string="Zufriedenheit")
    tco = fields.Float(string="TCO")
    clv = fields.Float(string="CLV")
    plot_data = fields.Text(string="Plot Daten")
