import pandas as pd
from .lgb_model import LGBModel


class PredictionResult:
    def __init__(self, input_data) -> None:
        self.output_df = None
        self.plot_output_df = pd.DataFrame()
        self.lgb_model = LGBModel()

        self.input_data = input_data
        ki_methods_count = input_data.get('ki_methods_count')
        # Use a default value if ki_methods_count is None
        self.count_methods_output = int(ki_methods_count) if ki_methods_count is not None else 1

    def create_result_df(self) -> None:
        input_data = self.lgb_model.prepare_input_data(self.input_data)
        prediction = self.lgb_model.predict(input_data)
        prediction_reverted = prediction[::-1]
        self.output_df = prediction_reverted[:self.count_methods_output]
        return

    def create_result_plot(self) -> None:
        self.plot_output_df = self.lgb_model.get_predict_result_plot(input_data=self.input_data)
        self.plot_output_df = self.plot_output_df[:self.count_methods_output]
        return
