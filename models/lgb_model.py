import os
import pickle
import pandas as pd


class LGBModel:
    def __init__(self):
        # Get the directory of the current file (user_input.py)
        current_dir = os.path.dirname(os.path.abspath(__file__))

        # Construct the path to the model file
        model_path = os.path.join(current_dir, '..', 'saved_model', 'lgbc.pkl')

        with open(model_path, 'rb') as file:
            self.model = pickle.load(file)

    @property
    def expected_column_order(self):
        return [
            'data_format_bild', 'data_format_keine', 'data_format_matrix',
            'data_format_text', 'data_format_ton', 'data_format_video',
            'data_quality_gering', 'data_quality_hoch', 'data_quality_keine',
            'data_quality_mittel', 'data_quality_sehr_hoch',
            'time_availability_gering', 'time_availability_hoch',
            'time_availability_mittel', 'time_availability_sehr_hoch',
            'accuracy_claim_gering', 'accuracy_claim_hoch',
            'accuracy_claim_mittel', 'accuracy_claim_sehr_hoch',
            'data_type_feedback_signal', 'data_type_gelabelt', 'data_type_gemischt',
            'data_type_nicht_gelabelt', 'type_goalsize_kategorisch',
            'type_goalsize_keine', 'type_goalsize_numerisch',
            'computing_power_gering', 'computing_power_hoch',
            'computing_power_mittel', 'computing_power_sehr_hoch',
            'data_amount_gross', 'data_amount_keine', 'data_amount_klein',
            'data_amount_mittel', 'data_amount_sehr_gross',
            'sequence_of_decisions_ja', 'sequence_of_decisions_nein',
            'dimensions_amount_gering', 'dimensions_amount_mittel',
            'dimensions_amount_hoch'
        ]

    @property
    def selection_field_mappings(self):
        return {
            'data_format': ['bild', 'text', 'ton', 'video', 'matrix', 'keine'],
            'data_quality': ['sehr_hoch', 'hoch', 'mittel', 'gering', 'keine'],
            'data_type': ['gelabelt', 'nicht_gelabelt', 'gemischt', 'feedback_signal'],
            'type_goalsize': ['numerisch', 'kategorisch', 'keine'],
            'accuracy_claim': ['gering', 'normal', 'hoch', 'sehr_hoch'],
            'dimensions_amount': ['hoch', 'mittel', 'gering'],
            'computing_power': ['gering', 'mittel', 'hoch', 'sehr_hoch'],
            'time_availability': ['gering', 'normal', 'hoch', 'sehr_hoch'],
            'data_amount': ['sehr_gross', 'gross', 'mittel', 'klein', 'keine'],
        }

    def predict(self, input_data):
        prediction = self.model.predict_proba(input_data)
        return self.model.classes_[prediction.argsort()[0]]

    def prepare_input_data(self, vals):
        # Initialize a dictionary to hold the transformed data
        transformed_data = {col: 0 for col in self.expected_column_order}

        # Handle selection fields
        for field, options in self.selection_field_mappings.items():
            for option in options:
                column_name = f"{field}_{option}"
                if vals.get(field) == option:
                    transformed_data[column_name] = 1

        # Handle boolean and integer fields only if they are in expected columns
        for field in ['sequence_of_decisions', 'ki_methods_count']:
            if f"{field}_ja" in transformed_data:
                transformed_data[f"{field}_ja"] = int(vals.get(field, False))
                transformed_data[f"{field}_nein"] = int(not vals.get(field, False))

        # Convert to DataFrame and reorder columns to match the model's training data
        final_df = pd.DataFrame([transformed_data])[self.expected_column_order]
        return final_df
