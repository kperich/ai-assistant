from odoo import models, fields, api
from .prediction_results import PredictionResult


class UserInput(models.Model):
    _name = 'ai_assistant.model_user_input'
    _description = 'Benutzereingaben'

    customer_id = fields.Char(string="Customer ID")
    customer_name = fields.Char(string="Customer Name")
    customer_loc = fields.Char(string="Location")
    customer_contact = fields.Char(string="E-Mail")

    data_format = fields.Selection([
        ('bild', 'Bild'),
        ('text', 'Text'),
        ('ton', 'Ton'),
        ('video', 'Video'),
        ('matrix', 'Matrix'),
        ('keine', 'Keine')
    ], string="Datenformat")
    data_quality = fields.Selection([
        ('sehr_hoch', 'Sehr Hoch'),
        ('hoch', 'Hoch'),
        ('mittel', 'Mittel'),
        ('gering', 'Gering'),
        ('keine', 'Keine')
    ], string="Datenqualität")
    data_type = fields.Selection([
        ('gelabelt', 'Gelabelt'),
        ('nicht_gelabelt', 'Nicht gelabelt'),
        ('gemischt', 'Gemischt'),
        ('feedback_signal', 'Feedback-Signal')
    ], string="Datentyp")
    data_amount = fields.Selection([
        ('sehr_gross', 'Sehr Groß'),
        ('gross', 'Groß'),
        ('mittel', 'Mittel'),
        ('klein', 'Klein'),
        ('keine', 'Keine')
    ], string="Datenmenge")
    type_goalsize = fields.Selection([
        ('numerisch', 'Numerisch'),
        ('kategorisch', 'Kategorisch'),
        ('keine', 'Keine')
    ], string="Typ der Zielgröße")
    accuracy_claim = fields.Selection([
        ('gering', 'Gering'),
        ('normal', 'Normal'),
        ('hoch', 'Hoch'),
        ('sehr_hoch', 'Sehr Hoch')
    ], string="Anspruch auf Genauigkeit")
    dimensions_amount = fields.Selection([
        ('hoch', 'Hoch'),
        ('mittel', 'Mittel'),
        ('gering', 'Gering')
    ], string="Anzahl an Dimensionen (Features)")
    computing_power = fields.Selection([
        ('gering', 'Gering'),
        ('mittel', 'Mittel'),
        ('hoch', 'Hoch'),
        ('sehr_hoch', 'Sehr Hoch')
    ], string="Rechenkapazität")
    time_availability = fields.Selection([
        ('gering', 'Gering'),
        ('normal', 'Normal'),
        ('hoch', 'Hoch'),
        ('sehr_hoch', 'Sehr Hoch')
    ], string="Verfügbarkeit von Zeit")
    ki_methods_count = fields.Integer(string="Anzahl der ausgegebenen KI-Methoden")

    result_output = fields.Text(string="Ergebnis")


    
    def action_redirect_to_ai_assistant(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_url',
            'url': '/ai_assistant',
            'target': 'self',
        }