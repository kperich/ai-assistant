from odoo import models, fields

class Query(models.Model):
    _name = 'ai_assistant.model_query'
    _description = 'History of all user queries'

    datum = fields.Date(string="Datum")
    user_input_id = fields.Many2one('ai_assistant.model_user_input', string="Benutzereingabe")
    results_id = fields.Many2one('ai_assistant.model_results_model', string="Ergebnisse")
